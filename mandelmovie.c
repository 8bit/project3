#include "bitmap.h"
#include "mandel.h"
#include <syscall.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>

#define MANDEL_PROCESS 50
// $ mandel -x 0.286932 -y 0.014287 -s .00005 -m 800 -W 3000 -H 3000

int main(int argc, char *argv[]) {
  if (argc < 2) {
    printf("pass # of processes as an argument\n");
    exit(1);
  }
  int limit = atoi(argv[1]);
  int eye = 0;
  int active = 0;
  int total = 0;
	double target = .00005;
	double start = 2;
  double step = (start - target) / MANDEL_PROCESS;
	while (total < MANDEL_PROCESS) {
    if (active == limit) {
      printf("Waiting on child\n");
      wait(NULL);
      active--;
    }
    int rc = fork();
    active++;
    if (rc < 0) {
      printf("fork failure\n");
      exit(1);
    }
    else if (rc == 0) {
      char buf[BUFSIZ];
      char buf2[BUFSIZ];
      sprintf(buf, "%f", start);
      sprintf(buf2, "/tmp/cattim%d.bmp", eye);
      char *argv[] = {"./mandel", "-x", "0.286932", "-y", "0.014287", "-s", buf, "-m", "800", "-W", "3000", "-H", "3000", "-o", buf2};
      if (argc == 3) { //Threads was passed as argument, use Thread eval B
        char *argv[] = {"./mandel", "-x", "0.2869325", "-y", "0.0142905", "-s", ".000001", "-W", "1024", "-H", "1024", "-m", "1000", "-o", buf2, "-n", argv[2]};
      }
      execvp("./mandel", argv);
    }
    start = start - step;
    eye++;
    total++;
  }
	return 0;
}

