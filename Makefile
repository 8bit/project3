# DO NOT EDIT -------------------------------
C=/afs/nd.edu/user14/csesoft/new/bin/gcc
CFLAGS=-Wall --std=gnu11
CPP=/afs/nd.edu/user14/csesoft/new/bin/g++
CPPFLAGS=-Wall
LD=/afs/nd.edu/user14/csesoft/new/bin/gcc
LDFLAGS=-lpthread
# # ---------------------------- END DO NOT EDIT

CPPFLAGS += -std=c++11 -g   # Add your own flags here, or leave blank
LDFLAGS +=                  # Add your own flags here, or leave blank

all: mandel mandelmovie

mandelmovie: mandelmovie.o bitmap.o
	$(C) $^ $(CFLAGS) -o $@

mandel: mandel.o bitmap.o
	$(LD) $^ $(LDFLAGS) -o $@

%.o: %.cpp
	$(CPP) $(CPPFLAGS) -c $<

bitmap.o: bitmap.c
	$(C) $(CFLAGS) -c $<

# Uncoment to use the C compiler
%.o: %.c
	$(C) $(CFLAGS) -c $<

.PHONY: clean all
clean:
	rm -f mandel mandelmovie *.o 

