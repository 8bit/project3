#include "bitmap.h"

#include <getopt.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <pthread.h>
#include <errno.h>
#include <string.h>

struct pArgs {
  struct bitmap *bm;
  double xmin;
  double xmax;
  double ymin;
  double ymax;
  int max;
  int start;
  int end;
};

int min(int x, int y);
int iteration_to_color(int i, int max );
int iterations_at_point(double x, double y, int max);
void *compute_image(void * pa);

void show_help() {
  printf("Use: mandel [options]\n");
  printf("Where options are:\n");
  printf("-m <max>    The maximum number of iterations per point. (default=1000)\n");
  printf("-n <threads>    The number of threads\n");
  printf("-x <coord>  X coordinate of image center point. (default=0)\n");
  printf("-y <coord>  Y coordinate of image center point. (default=0)\n");
  printf("-s <scale>  Scale of the image in Mandlebrot coordinates. (default=4)\n");
  printf("-W <pixels> Width of the image in pixels. (default=500)\n");
  printf("-H <pixels> Height of the image in pixels. (default=500)\n");
  printf("-o <file>   Set output file. (default=mandel.bmp)\n");
  printf("-h          Show this help text.\n");
  printf("\nSome examples are:\n");
  printf("mandel -x -0.5 -y -0.5 -s 0.2\n");
  printf("mandel -x -.38 -y -.665 -s .05 -m 100\n");
  printf("mandel -x 0.286932 -y 0.014287 -s .0005 -m 1000\n\n");
}

int main(int argc, char *argv[]) {
  char c;

  // These are the default configuration values used
  // if no command line arguments are given.

  const char *outfile = "mandel.bmp";
  double xcenter = 0;
  double ycenter = 0;
  double scale = 4;
  int    image_width = 500;
  int    image_height = 500;
  int    max = 1000;
  int    threads = 1;

  // For each command line argument given,
  // override the appropriate configuration value.

  while ((c = getopt(argc, argv, "x:y:n:s:W:H:m:o:h"))!=-1) {
    switch(c) {
      case 'x':
        xcenter = atof(optarg);
        break;
      case 'n':
        threads = atof(optarg);
        break;
      case 'y':
        ycenter = atof(optarg);
        break;
      case 's':
        scale = atof(optarg);
        break;
      case 'W':
        image_width = atoi(optarg);
        break;
      case 'H':
        image_height = atoi(optarg);
        break;
      case 'm':
        max = atoi(optarg);
        break;
      case 'o':
        outfile = optarg;
        break;
      case 'h':
        show_help();
        exit(1);
        break;
    }
  }

  // Display the configuration of the image.
  printf("mandel: x=%lf y=%lf scale=%lf max=%d outfile=%s\n", xcenter, ycenter, scale, max, outfile);

  // Create a bitmap of the appropriate size.
  struct bitmap *bm = bitmap_create(image_width, image_height);

  // Fill it with a dark blue, for debugging
  bitmap_reset(bm, MAKE_RGBA(0, 0, 255, 0));

  // Compute the Mandelbrot image
  int step = (image_height / threads) + 1;
  int at = 0;
  struct pArgs * pas[threads];
  pthread_t * my_threads = malloc(threads * sizeof(pthread_t));
  if (!my_threads) {
    printf("Malloc error.\n");
    exit(10);
  }
  for (int i = 0; i < threads; i++) {
    struct pArgs * pa = malloc(sizeof(struct pArgs));
    if (!pa) {
      printf("Malloc error.\n");
      exit(10);
    }
    pas[i] = pa;
    pa->bm = bm;
    pa->xmin = xcenter-scale;
    pa->xmax = xcenter+scale;
    pa->ymin = ycenter-scale;
    pa->ymax = ycenter+scale;
    pa->start = at;
    pa->end = at + step;
    pa->max = max;
    int rc = pthread_create(&my_threads[i], NULL, compute_image, (void *)pa);
    if (rc != 0) {
      printf("Pthread create error\n");
      exit(10);
    }
    at += step;
  }
  for (int i = 0; i < threads; i++) {
    pthread_join(my_threads[i], NULL);
    free(pas[i]);
  }

  // Save the image in the stated file.
  if(!bitmap_save(bm,outfile)) {
    fprintf(stderr, "mandel: couldn't write to %s: %s\n", outfile, strerror(errno));
    return 1;
  }

  return 0;
}

/*
Compute an entire Mandelbrot image, writing each point to the given bitmap.
Scale the image to the range (xmin-xmax,ymin-ymax), limiting iterations to "max"
*/

void *compute_image(void * pav) {
  struct pArgs * pa = (struct pArgs *)pav;
  struct bitmap * bm = pa->bm;
  double xmin = pa->xmin;
  double xmax = pa->xmax;
  double ymin = pa->ymin;
  double ymax = pa->ymax;
  double start = pa->start;
  double end = pa->end;
  int max = pa->max;
  int i, j;

  int width = bitmap_width(bm);
  int height = bitmap_height(bm);
  if (start >= height) return NULL;

  // For every pixel in the image...

  for(j = start; j < min(end, height); j++) {

    for(i = 0; i < width; i++) {

      // Determine the point in x,y space for that pixel.
      double x = xmin + i*(xmax-xmin)/width;
      double y = ymin + j*(ymax-ymin)/height;

      // Compute the iterations at that point.
      int iters = iterations_at_point(x, y ,max);

      // Set the pixel in the bitmap.
      bitmap_set(bm, i, j, iters);
    }
  }
  return NULL;
}

int min(int x, int y) {
  if (x > y) return y;
  return x;
}

/*
Return the number of iterations at point x, y
in the Mandelbrot space, up to a maximum of max.
*/
int iterations_at_point(double x, double y, int max) {
  double x0 = x;
  double y0 = y;

  int iter = 0;

  while ((x*x + y*y <= 4) && iter < max) {

    double xt = x*x - y*y + x0;
    double yt = 2*x*y + y0;

    x = xt;
    y = yt;

    iter++;
  }

  return iteration_to_color(iter, max);
}

/*
Convert a iteration number to an RGBA color.
Here, we just scale to gray with a maximum of imax.
Modify this function to make more interesting colors.
*/

int iteration_to_color(int i, int max) {
  //int r = ((i) % 50)*4;
  //int g = ((i) % 70);
  //int b = ((i) % 200);
  int r = (i*i % 165);
  int g = (i % 220);
  int b = (i % 230);
  return MAKE_RGBA(r, g, b, 0);
}
