I was running on student00 for every experiment.

process evaluation:
-------

"./mandelmovie 1"
real  33m21.725s
user  33m18.427s
sys 0m2.424s

"./mandelmovie 2"
real  12m31.314s
user  21m13.363s
sys 0m2.755s

"./mandelmovie 3"
real  9m57.707s
user  26m39.241s
sys 0m2.927s

"./mandelmovie 4"
real  6m48.860s
user  23m20.518s
sys 0m2.919s

"./mandelmovie 5"
real	6m48.453s
user	32m1.245s
sys	0m2.856s

"./mandelmovie 6"
real	5m37.129s
user	30m52.429s
sys	0m3.179s


thread evaluation:
-------------
A:
"./mandel -x -.5 -y .5 -s 1 -m 2000 -n 1"
real	0m2.563s
user	0m2.530s
sys	0m0.007s

"./mandel -x -.5 -y .5 -s 1 -m 2000 -n 2"
real	0m2.303s
user	0m2.524s
sys	0m0.005s

"./mandel -x -.5 -y .5 -s 1 -m 2000 -n 4"
real	0m1.167s
user	0m2.518s
sys	0m0.004s

"./mandel -x -.5 -y .5 -s 1 -m 2000 -n 8"
real	0m0.718s
user	0m2.513s
sys	0m0.007s

"./mandel -x -.5 -y .5 -s 1 -m 2000 -n 12"
real	0m0.509s
user	0m2.526s
sys	0m0.002s

B:
"./mandel -x 0.2869325 -y 0.0142905 -s .000001 -W 1024 -H 1024 -m 1000 -n 1"
real	0m6.241s
user	0m6.186s
sys	0m0.016s

"./mandel -x 0.2869325 -y 0.0142905 -s .000001 -W 1024 -H 1024 -m 1000 -n 2"
real	0m3.299s
user	0m6.174s
sys	0m0.019s

"./mandel -x 0.2869325 -y 0.0142905 -s .000001 -W 1024 -H 1024 -m 1000 -n 4"
real	0m1.820s
user	0m6.168s
sys	0m0.012s

"./mandel -x 0.2869325 -y 0.0142905 -s .000001 -W 1024 -H 1024 -m 1000 -n 8"
real	0m1.001s
user	0m6.170s
sys	0m0.018s

"./mandel -x 0.2869325 -y 0.0142905 -s .000001 -W 1024 -H 1024 -m 1000 -n 12"
real	0m0.710s
user	0m6.157s
sys	0m0.022s

Combination:
------------
"./mandelmovie 2 1"
1 thread, 2 processes
real	15m3.284s
user	29m36.426s
sys	0m3.292s

"./mandelmovie 2 2"
2 threads, 2 processes
real  21m56.203s
user  29m8.362s
sys 0m2.646s

"./mandelmovie 2 4"
4 threads, 2 processes
real  22m16.851s
user  29m7.902s
sys 0m3.121s
